<%@page import="java.nio.charset.*" %>
<%@page import="java.io.*" %>

<%
    ServletInputStream inputStream = request.getInputStream();


    StringBuilder stringBuilder = new StringBuilder();
    String line = null;

    try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, Charset.defaultCharset()))) {	
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line);
        }
    }

    out.println(stringBuilder.toString());
%>
