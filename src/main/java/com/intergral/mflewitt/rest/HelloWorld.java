package com.intergral.mflewitt.rest;

import javax.ws.rs.*;

@Path( "basic" )
public class HelloWorld
{
    @GET
    @Path( "hello" )
    public String Helloworld()
    {
        System.out.println(System.getProperty("java.io.tmpdir"));
        return "hello world";
    }

    @GET
    @Path( "helloname/{name}" )
    public String helloname(@PathParam( "name" ) final String name)
    {
        return "hello " + name;
    }
}
