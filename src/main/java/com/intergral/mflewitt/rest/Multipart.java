package com.intergral.mflewitt.rest;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import org.apache.commons.io.IOUtils;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

@Path( "multi" )
public class Multipart
{
    @Path("form")
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response uploadPost( MultipartFormDataInput input) throws IOException
    {
        Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
        List<InputPart> inputParts = uploadForm.get("uploadedFile");

        for (InputPart inputPart : inputParts) {
//            InputStream inputStream = inputPart.getBody( InputStream.class,null);
//
//            byte [] bytes = IOUtils.toByteArray(inputStream);

            System.out.println("horse");
        }
        return Response.ok().entity( "file processing complete" ).build();
    }
}
